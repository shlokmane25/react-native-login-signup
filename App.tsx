/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */

import React from 'react';
import {
  SafeAreaView,
} from 'react-native';
import WelcomeScreen from './Screens/WelcomeScreen';

function App(): React.JSX.Element {
  return (
    <SafeAreaView className='flex-1'>
      <WelcomeScreen />
    </SafeAreaView>
  );
}



export default App;
